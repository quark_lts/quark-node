package node

import (
	"encoding/json"
	"fmt"
	"log"
	"sync"

	"github.com/fasthttp/router"
	"github.com/quark_lt/pkg/metrics"
	"github.com/quark_lt/pkg/util/config"
	"github.com/sirupsen/logrus"
	"github.com/valyala/fasthttp"
	"github.com/valyala/gorpc"

	"gitlab.com/quark-node/cmd/db_worker"
)

type QuarkNode struct {
	Wg        *sync.WaitGroup
	Db        *db_worker.DbWorker
	Scheduler *NodeWorkerScheduler
	Config    *config.QuarkNodeConfig
}

func NewQuarkNode(conf *config.QuarkNodeConfig) *QuarkNode {
	return &QuarkNode{
		Wg:        &sync.WaitGroup{},
		Db:        db_worker.NewDbWorker(),
		Config:    conf,
		Scheduler: NewNodeWorkerScheduler(),
	}
}
func ParseMetrics(data interface{}) *metrics.Metrics {
	es, _ := data.(metrics.Metrics)
	return &es
}
func (node *QuarkNode) InitRpcServer() {
	gorpc.RegisterType(metrics.Metrics{})
	s := &gorpc.Server{
		// Accept clients on this TCP address.
		Addr: node.Config.ServerConfig.Host + ":9999",
		// Echo handler - just return back the message we received from the client
		Handler: func(clientAddr string, request interface{}) interface{} {
			logrus.Println(clientAddr)
			shh := node.Scheduler.GetMetrics()
			rps := ParseMetrics(request)
			node.Db.WriteMetrics(shh, rps)

			return nil
		},
	}
	if err := s.Start(); err != nil {
		log.Fatalf("Cannot start rpc server: %s", err)
	}
}
func (node *QuarkNode) InitCoreServer() {
	rt := router.New()
	rt.POST("/metrics", func(ctx *fasthttp.RequestCtx) {

		metrics := metrics.Metrics{}
		sshMetrics := node.Scheduler.GetMetrics()
		err := json.Unmarshal(ctx.PostBody(), &metrics)
		if err != nil {
			panic(err)
		}
		go node.Db.WriteMetrics(sshMetrics, &metrics)
	})
	rt.POST("/start", func(ctx *fasthttp.RequestCtx) {

		conf := config.QuarkLTConfig{}
		err := json.Unmarshal(ctx.PostBody(), &conf)
		if err != nil {
			logrus.Fatalln(err)
		}
		logrus.Infoln("QuarkNode success to start QuarkltConfig")

		node.Scheduler.AddWorker(node.Config, &conf)
		if conf.SiteSetup.Helpers.SshAgent != nil {
			node.Scheduler.AddAgent(conf.SiteSetup.Helpers.SshAgent)

		}
		node.Scheduler.Start()

		ctx.WriteString("ok")

	})
	rt.POST("/stop", func(ctx *fasthttp.RequestCtx) {
		node.Scheduler.Lock()
		node.Scheduler.Stop()
		logrus.Infoln("QuarkNode stopped  QuarkltConfig")

		node.Scheduler.Unlock()

	})
	rt.GET("/stats", func(ctx *fasthttp.RequestCtx) {
		//todo: добавить статистику по планировщику
		node.Scheduler.Lock()
		fmt.Println(node.Scheduler)
		ctx.Write(node.Scheduler.GetJson())
		logrus.Infoln("Request to get stats ", config.ParseToString(node.Scheduler))
		node.Scheduler.Unlock()
	})

	logrus.Infoln("Quark Node is successfully started")
	logrus.Infoln("QuarkNodeScheduler  is successfully started")
	logrus.Infoln("Quark Node is active on http://localhost:7777")

	err := fasthttp.ListenAndServe(":7777", rt.Handler)

	if err != nil {

		logrus.Fatalln(err)
	}

}
