package node

import (
	"encoding/json"
	"fmt"
	"log"
	"sync"

	"github.com/quark_lt/pkg/metrics"
	"github.com/quark_lt/pkg/util/config"
	"github.com/quark_lt/pkg/util/uuid"

	"gitlab.com/quark-node/cmd/agents"
)

type NodeWorkerScheduler struct {
	sync.Mutex
	Target        string                      `json:"target"`
	Workers       map[string]*agents.Worker   `json:"workers"`
	Agents        map[string]*agents.SshAgent `json:"agents"`
	WorkerCounter int                         `json:"workerCount"`
	AgentCounter  int                         `json:"agentCount"`
}

func NewNodeWorkerScheduler() *NodeWorkerScheduler {
	return &NodeWorkerScheduler{
		Workers:       map[string]*agents.Worker{},
		Agents:        map[string]*agents.SshAgent{},
		WorkerCounter: 0,
		AgentCounter:  0,
	}
}
func (scheduler *NodeWorkerScheduler) AddAgent(conf *config.SshAgentConf) {
	id := uuid.GenerateUuid()
	scheduler.Lock()
	scheduler.Agents[id] = agents.NewSshAgent(8000+scheduler.AgentCounter, conf)
	scheduler.AgentCounter++
	scheduler.Unlock()

}
func (scheduler *NodeWorkerScheduler) AddWorker(conf *config.QuarkNodeConfig, quark *config.QuarkLTConfig) {
	log.Println(config.ParseToString(conf))
	workerCfg := config.WorkerConfig{ServerConfig: &config.ServerConfig{}}
	workerCfg.ExporterUrl = fmt.Sprintf("%s:%s", conf.ServerConfig.Host, conf.ServerConfig.Port)
	workerCfg.ServerConfig.Host = conf.ServerConfig.Host
	workerCfg.ServerConfig.Port = fmt.Sprintf("%s", 5000+scheduler.WorkerCounter)
	workerCfg.Config = quark.SiteSetup.Schedules[0]
	workerCfg.WorkerType = config.EXTERNAL_TYPE

	scheduler.Lock()
	scheduler.Workers[uuid.GenerateUuid()] = &agents.Worker{
		Config: &workerCfg,
	}
	scheduler.WorkerCounter++
	scheduler.Unlock()
}

func (scheduler *NodeWorkerScheduler) StopAgent() {

	for k, v := range scheduler.Agents {

		v.Close()
		delete(scheduler.Agents, k)
	}
}

func (scheduler *NodeWorkerScheduler) StopWorker() {
	for k, v := range scheduler.Workers {

		v.Close()
		delete(scheduler.Workers, k)
	}
}

func (scheduler *NodeWorkerScheduler) GetMetrics() *metrics.SSHMetrics {

	for _, helper := range scheduler.Agents {
		return helper.GetMetrics()
	}
	return nil
}

func (scheduler *NodeWorkerScheduler) GetRpcMetrics() *metrics.SSHMetrics {
	for _, helper := range scheduler.Agents {
		data := helper.GetMetrics()
		return data
	}
	return nil

}
func (scheduler *NodeWorkerScheduler) Start() bool {
	log.Println("error run")
	for _, v := range scheduler.Workers {
		go v.Run()
	}
	for _, v := range scheduler.Agents {
		v.Run()
	}
	return true
}
func (scheduler *NodeWorkerScheduler) Stop() bool {
	for _, v := range scheduler.Workers {
		go v.Close()
	}
	for _, v := range scheduler.Agents {
		go v.Close()
	}
	return true
}
func (scheduler *NodeWorkerScheduler) GetJson() []byte {
	data, err := json.Marshal(scheduler)
	if err != nil {
		log.Fatal(err)
	}
	return data
}
