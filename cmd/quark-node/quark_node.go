package main

import (
	"os"

	"github.com/quark_lt/pkg/util/config"

	node2 "gitlab.com/quark-node/cmd/quark-node/node"
)

func main() {
	args := os.Args[1:]
	file := config.ParseQuarkNodeConfigFile(args[0])
	node := node2.NewQuarkNode(file)

	node.InitRpcServer()
	node.InitCoreServer()

}
