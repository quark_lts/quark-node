package agents

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/quark_lt/pkg/util/config"
	"github.com/sirupsen/logrus"

	"gitlab.com/quark-node/cmd/std"
)

type AbstractWorker interface {
	Close()
	HealthCheck()
	Run()
}
type Worker struct {
	Config *config.WorkerConfig `json:"config"`
}

func (w *Worker) Run() {
	//todo: добавить глобальный путь для запуска рабочих

	std.ExecPart(
		"./quark_worker",
		config.ParseToString(w.Config),
	)
	client := http.Client{
		Timeout: 2 * time.Second,
	}
	_, err := client.Get(fmt.Sprintf("%s:%s/start", w.Config.ServerConfig.Host, w.Config.ServerConfig.Port))
	if err != nil {
		log.Println("start success")
	}
}

func (w *Worker) Close() {
	_, err := http.Get(fmt.Sprintf("%s:%s/stop", w.Config.ServerConfig.Host, w.Config.ServerConfig.Port))
	if err != nil {
		logrus.Warnf("Close Worker is success %v", err)
	}

}
