package agents

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/quark_lt/pkg/metrics"
	"github.com/quark_lt/pkg/util/config"
	"github.com/sirupsen/logrus"
	"github.com/valyala/gorpc"

	"gitlab.com/quark-node/cmd/std"
)

type AbstractAgent interface {
	GetMetrics() interface{}
	Close()
	Run()
}

type SshAgent struct {
	Port   int                  `json:"port"`
	Config *config.SshAgentConf `json:"config"`
	Client *gorpc.Client
}

func NewSshAgent(port int, config *config.SshAgentConf) *SshAgent {
	return &SshAgent{
		Port:   port,
		Config: config,
		Client: &gorpc.Client{
			Addr: fmt.Sprintf(":%d", port),
		},
	}
}

func (s *SshAgent) Run() {
	std.ExecSsh("./quark_agent", fmt.Sprintf("%d", s.Port), config.ParseToString(s.Config))
	defer s.Client.Start()
}

func (s *SshAgent) Close() {

	_, err := http.Post(fmt.Sprintf("http://localhost:%d/stop", s.Port), "plain/text", bytes.NewBuffer([]byte("close")))
	if err != nil {
		logrus.Warnf("success %v", err)
	}

}

func (s *SshAgent) GetMetrics() *metrics.SSHMetrics {
	res, err := http.Get(fmt.Sprintf("http://localhost:%d/ssh", s.Port))
	if res != nil && res.StatusCode == 200 && err == nil {

		body, err := ioutil.ReadAll(res.Body)
		m := metrics.SSHMetrics{}
		err = json.Unmarshal(body, &m)
		if err != nil {
			log.Println(err)

		}
		return &m
	}
	return nil

}
