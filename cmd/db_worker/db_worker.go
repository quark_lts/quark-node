package db_worker

import (
	"fmt"
	_ "github.com/influxdata/influxdb1-client" // this is important because of the bug in go mod
	client "github.com/influxdata/influxdb1-client/v2"
	"github.com/quark_lt/pkg/metrics"
	"time"
)

type DbWorker struct {
	Client       client.Client
	GunServer    string
	TargetServer string
}

func NewDbWorker() *DbWorker {
	c, err := client.NewHTTPClient(client.HTTPConfig{
		Addr: "http://localhost:8086",
	})
	if err != nil {
		panic(err)
	}

	return &DbWorker{Client: c, GunServer: "localhost", TargetServer: "localhost"}
}
func (worker *DbWorker) WriteMetrics(sshMetrics *metrics.SSHMetrics, targetMetrics *metrics.Metrics) {
	//if sshMetrics == nil {
	//	return
	//}
	bp, _ := client.NewBatchPoints(client.BatchPointsConfig{
		Database:  "quarklt",
		Precision: "s",
	})
	tags := map[string]string{
		"target_server": worker.TargetServer,
		"gun_server":    worker.GunServer,
	}
	fields := map[string]interface{}{
		//"memory":        sshMetrics.MemoryInfo.Total,
		//"memory-free":   sshMetrics.MemoryInfo.Free,
		//"memory-cached": sshMetrics.MemoryInfo.Caches,
		//"memory-used":   sshMetrics.MemoryInfo.Used,
		//"cpu-load":      sshMetrics.CpuLoad,
		//"disk-load":     sshMetrics.IoLoad,
		"responseTime": targetMetrics.ResponseTime,
		"responseCode": targetMetrics.ResponseCode,
	}

	flexTime := time.Now()
	pt, err := client.NewPoint(
		"gun-metrics",
		tags,
		fields,
		flexTime,
	)

	bp.AddPoint(pt)
	if sshMetrics != nil {
		fmt.Println(sshMetrics)
		pt, err = client.NewPoint(
			"ssh-metrics",
			tags,
			map[string]interface{}{
				"memory":        sshMetrics.MemoryInfo.Total,
				"memory-free":   sshMetrics.MemoryInfo.Free,
				"memory-cached": sshMetrics.MemoryInfo.Caches,
				"memory-used":   sshMetrics.MemoryInfo.Used,
				"cpu-load":      sshMetrics.CpuLoad,
				"disk-load":     sshMetrics.IoLoad,
			},
			flexTime,
		)
		bp.AddPoint(pt)
	}

	if err != nil {
		panic(err)
	}
	worker.Client.Write(bp)

}
func (worker *DbWorker) WriteMetricWithoutAgent(targetMetrics *metrics.Metrics) {
	bp, _ := client.NewBatchPoints(client.BatchPointsConfig{
		Database:  "quarklt-node",
		Precision: "s",
	})
	tags := map[string]string{
		"target-server": worker.TargetServer,
		"gun-server":    worker.GunServer,
	}
	fields := map[string]interface{}{
		"responseTime": targetMetrics.ResponseTime.String(),
		"responseCode": targetMetrics.ResponseCode,
	}
	pt, err := client.NewPoint(
		worker.TargetServer,
		tags,
		fields,
		time.Now())
	bp.AddPoint(pt)
	if err != nil {
		panic(err)
	}
	worker.Client.Write(bp)
}
