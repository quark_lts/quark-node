package std

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"

	"github.com/sirupsen/logrus"
)

func ExecPart(program string, conf string) {
	path, err := os.Getwd()

	cmd := exec.Command(program, conf)
	cmd.Dir = path
	logrus.Println(conf)
	var stderr bytes.Buffer
	cmd.Stderr = &stderr
	logrus.Println("Running Worker command and waiting for it to finish...")

	err = cmd.Run()

	logrus.Warnf("Worker-[%s] exit with err <==%v", stderr.String(), err)

}
func ExecSsh(program string, port string, conf string) {

	path, err := os.Getwd()

	var stderr bytes.Buffer
	cmd := exec.Command(program, port, conf, "/bin/bash")
	cmd.Dir = path
	cmd.Stderr = &stderr

	logrus.Println("Running Agent command and waiting for it to finish...")

	err = cmd.Run()
	fmt.Println(fmt.Sprint(err) + ": " + stderr.String())
	logrus.Warnf("SSH_Agent %v:%s", err, stderr.String())
	logrus.Warnf("Command finished with error: %v", err)

}
